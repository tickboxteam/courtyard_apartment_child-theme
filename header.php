<?php

$THEME_SETTINGS = nirvana_get_theme_options();


/**
 * The Header
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package Cryout Creations
 * @subpackage nirvana
 * @since nirvana 0.5
 */
 ?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php  cryout_meta_hook(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
 	cryout_header_hook();
	wp_head(); ?>

<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5477056"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>
<noscript><img src="//bat.bing.com/action/0?ti=5477056&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>
</head>
<body <?php body_class(); ?>>

<?php cryout_body_hook(); ?>

<div id="wrapper" class="hfeed">
<?php cryout_wrapper_hook(); ?>

<div id="header-full">
	<header id="header">
		<div id="masthead">
		<?php cryout_masthead_hook(); ?>
			<div id="branding" role="banner">
				<span class="logo-wrap">
					<a title="<?php echo get_bloginfo( 'name' ); ?>" href="/">
						<img class="main-logo" alt="<?php echo get_bloginfo( 'name' ); ?>" src="/wp-content/uploads/2020/07/Courtyard_logo.png" height="100px"></img>
					</a>
				</span>

					<div class="header-container ">
						<div class="site_dets">
							<h1 id="site-title">
							<a rel="home" title="<?php echo get_bloginfo( 'name' ); ?>" href="/"><?php echo get_bloginfo( 'name' ); ?></a></h1>
							<div id="site-description"><?php echo get_bloginfo( 'description' ); ?>
							</div>
						</div>

						<span class="logos">
							<ul>
								<li><img alt="" src="/wp-content/uploads/2020/07/walkers.jpg"></li>
								<li><img alt="" src="/wp-content/uploads/2020/07/star.jpg"></li>
							</ul>
	            		</span>
					</div>
				<div style="clear:both;"></div>
			</div><!-- #branding -->
			<a id="nav-toggle"><span>&nbsp;</span></a>
			<nav id="access" role="navigation">
				<?php cryout_access_hook();?>
			</nav><!-- #access -->
			
			
		</div><!-- #masthead -->
	</header><!-- #header -->
</div><!-- #header-full -->

<div style="clear:both;height:0;"> </div>
<?php cryout_breadcrumbs_hook();?>
<div id="main">
		<?php cryout_main_hook(); ?>
	<div  id="forbottom" >
		<?php cryout_forbottom_hook(); ?>

		<div style="clear:both;"> </div>